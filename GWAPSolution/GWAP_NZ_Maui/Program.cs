﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GWAP_NZ_Maui
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new NZ_Maui() 
            //};
            //ServiceBase.Run(ServicesToRun);
#if(!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
	   { 
	        new NZ_Maui(),  
	   };
            ServiceBase.Run(ServicesToRun);
#else
            NZ_Maui myServ = new NZ_Maui();
            myServ.CreateMultipleTasks();
            // here Process is my Service function
            // that will run when my service onstart is call
            // you need to call your own method or function name here instead of Process();
#endif
        }
    }
}
