﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using GetAvailabilityWithPrice.Domain.Services;
using GetAvailabilityWithPrice.Infrastructure.Services;

namespace AU_Maui
{
    public partial class AU_Maui : ServiceBase
    {
        public AU_Maui()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CreateMultipleTasks();
        }

        private static void CreateMultipleTasks()
        {
            string brandCode = "B";
            IAvailabilityAndPriceDataService AAPDService = new AvailabilityAndPriceDataService(brandCode);
            AAPDService.PerformFetchAndPopulateDBTask();
        }
        protected override void OnStop()
        {
        }
    }
}
