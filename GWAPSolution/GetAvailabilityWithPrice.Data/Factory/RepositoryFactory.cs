﻿using GetAvailabilityWithPrice.Data.Repository;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Data.Factory
{
    public class RepositoryFactory:IRepositoryFactory
    {
        public static readonly IRepositoryFactory Instance = new RepositoryFactory();
        IParametersRepository parameterRepo;
        IAuroraRepository auroraRepo;
        IPhoenixWSRepository phoenixRepo;

        public IParametersRepository GetParametersRepository()
        {
            parameterRepo = (IParametersRepository)new ParametersRepository(ConfigurationManager.AppSettings["XMLPath"].ToString()); ;
            return parameterRepo;
        }

        public IAuroraRepository GetAuroraRepository()
        {
            string className = ConfigurationManager.ConnectionStrings["AuroraConnection"].ToString();
            auroraRepo = (IAuroraRepository)new AuroraRepository();
            return auroraRepo;
        }

        public IPhoenixWSRepository GetPhoenixWSRepository()
        {
            phoenixRepo = new PhoenixWSRepository();

            return phoenixRepo;
        }


        public IParametersRepository GetParametersRepository(string paramKey)
        {
            parameterRepo = (IParametersRepository)new ParametersRepository(ConfigurationManager.AppSettings[paramKey].ToString()); ;
            return parameterRepo;
        }
    }
}
