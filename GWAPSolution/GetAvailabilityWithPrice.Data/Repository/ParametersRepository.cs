﻿using GetAvailabilityWithPrice.Data.Models;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GetAvailabilityWithPrice.Data.Repository
{
    class ParametersRepository : IParametersRepository
    {
        public string RepoPath { get; set; }
        public ParametersRepository(string repoPath)
        {
            RepoPath = repoPath;
        }
        public IEnumerable<ParametersEntity> GetParamsList()
        {
            XDocument retDocument = XDocument.Load(RepoPath);
            IEnumerable<ParametersModel> retList = new List<ParametersModel>();

            retList = (from c in retDocument.Descendants("Locations").Descendants("Country")
                       select new ParametersModel
                       {
                           AgentCode = c.Attribute("AgentCode").Value,
                           Countrycode = c.Attribute("Code").Value,
                           BrandList = from b in c.Descendants("Brand")
                                       select new BrandModel
                                       {
                                           BrandCode = b.Attribute("Code").Value,
                                           LocationList = from l in b.Descendants("Location")
                                                          select new LocationModel
                                                          {
                                                              LocationFrom = l.Attribute("From").Value,
                                                              LocationTo = l.Attribute("To").Value
                                                          }
                                       }
                       }
                          ).ToList();

            return retList;
        }

    }
}
