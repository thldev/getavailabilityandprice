﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Configuration;
using System.Web.Services.Description;
using System.Xml;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using System.IO;
using SendGrid;


namespace GetAvailabilityWithPrice.Data.Repository
{

    /// <summary>
    /// Debug and logging functionality for the THL mapping platform
    /// </summary>
    public class THLDebugRepository
    {
        public static bool ErrorLogOn = Convert.ToBoolean(ConfigurationManager.AppSettings["ErrorLogging"]);
        public static bool EventLogOn = Convert.ToBoolean(ConfigurationManager.AppSettings["EventLogging"]);
        public static string SmtpServerName = ConfigurationManager.AppSettings["SmtpAddress"];
        public static string SmtpUserName = ConfigurationManager.AppSettings["SmtpUsername"];
        public static string SmtpPassword = ConfigurationManager.AppSettings["SmtpPassword"];
        public static string SmtpPort = ConfigurationManager.AppSettings["SmtpPort"];
        public static bool SmtpEnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["SmtpEnableSsl"]);

        public static string EmailTo = ConfigurationManager.AppSettings["emailTo"];
        public static string EmailFrom = ConfigurationManager.AppSettings["emailFrom"];

        static string logPath;
        static string eventPath;
        static string logFile;

        static THLDebugRepository()
        {
            logPath = ConfigurationManager.AppSettings["logPath"];
            eventPath = ConfigurationManager.AppSettings["eventPath"];
            logFile = ConfigurationManager.AppSettings["logFile"] + "_" + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + ".log"; ;
        }

        public static void SetFileName()
        {
            DateTime startTime = ConfigurationManager.AppSettings["CurrentDatetime"] == null ? DateTime.Now : DateTime.Parse(ConfigurationManager.AppSettings["CurrentDatetime"]);
            logFile = ConfigurationManager.AppSettings["logFile"] + "_" + startTime.Day + startTime.Month + startTime.Year + ".log"; ;
        }
        public static bool LogError(ErrorTypes errType, string errMethod, string errStr, string errParams)
        {
            if (ErrorLogOn == false) return false;

            EventLog ev = new EventLog("Application");
            ev.Source = "AvailabilityAndPriceData";

            //if (!EventLog.SourceExists("AvailabilityAndPriceData"))
            //    EventLog.CreateEventSource("AvailabilityAndPriceData", "AvailabilityAndPriceData");

            //try
            //{
            //    ev.WriteEntry(errType + ":" + errMethod + ":" + errStr, EventLogEntryType.Warning, DateTime.Now.Millisecond, 2);
            //    return true;//If that fails will write to FS log
            //}
            //catch (Exception ex)
            //{
            //    string eventWriteError = ex.Message;
            //}
            //ev = null;

            System.IO.StreamWriter sw = System.IO.File.AppendText(logPath + "AvailabilityAndPriceData.log");
            try
            {
                string logLine = DateTime.Now + ", " + errType + ", " + errMethod + ", " + errStr + ", " + errParams;
                Task theTask = sw.WriteLineAsync(logLine);
                //sw.WriteLine(logLine);
                theTask.Start();
                theTask.Wait();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                sw.Close();
            }
        }

        public static bool SendEmail(string MessageBody)
        {
            bool retValue = true;
            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.Host = SmtpServerName;
                    client.EnableSsl = SmtpEnableSsl;
                    client.Port = Convert.ToInt32(SmtpPort);
                    client.Timeout = 12000;
                    //if (true)
                    //{
                    //    client.UseDefaultCredentials = false;
                    //    NetworkCredential networkCredential = new NetworkCredential(SmtpUserName, SmtpPassword);
                    //    client.Credentials = networkCredential;
                    //}
                    MailMessage mailMessage = new MailMessage();
                    foreach (var emailto in EmailTo.Split(';'))
                    {
                        if (!emailto.Trim().Equals(string.Empty)) mailMessage.To.Add(new MailAddress(emailto));
                    }
                    mailMessage.From = new MailAddress(EmailFrom, "Service Outcome");
                    mailMessage.Subject = "From GetGetAvailabilityAndPrice";
                    mailMessage.Body = MessageBody;
                    client.Send(mailMessage);
                }
                retValue = true; // SendEmailResult.CreateSuccess();

            }
            catch (Exception ex)
            {
                retValue = false;
                LogError(ErrorTypes.ExternalProvider, "SendEmail", ex.Message, null);
            }


            return retValue;
        }

        public static bool SendOutcomeEmail()
        {
            bool retValue = true;
            string fileName = logPath + logFile;
            
            //try
            //{
            //    MailMessage mailMsg = new MailMessage();

            //    // To
            //    mailMsg.To.Add(new MailAddress(EmailTo, "Nimesh Trivedi"));

            //    // From
            //    mailMsg.From = new MailAddress("nimesh.trivedi@thlonline.com", "Nims");

            //    // Subject and multipart/alternative Body
            //    mailMsg.Subject = "From AvailabilityAndPrice" + logFile ;
            //    string text = "Please check attached file";
            //    string html = @"<p>Please Check the attached file</p>";
            //    mailMsg.Attachments.Add(new Attachment(fileName));
            //    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
            //    mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

            //    // Init SmtpClient and send

            //    SmtpClient smtpClient = new SmtpClient(SmtpServerName, Convert.ToInt32(SmtpPort));
            //    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(EmailFrom, SmtpPassword);
            //    smtpClient.Credentials = credentials;

            //    smtpClient.Send(mailMsg);
            //}
            //catch (Exception ex)
            //{
            //    retValue = false;
            //    LogError(ErrorTypes.ExternalProvider, "SendEmail", ex.Message, null);
            //}

            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.Host = SmtpServerName;
                    client.EnableSsl = SmtpEnableSsl;
                    client.Port = Convert.ToInt32(SmtpPort);
                    client.Timeout = 12000;
                    //if (true)
                    //{
                    //    client.UseDefaultCredentials = false;
                    //    NetworkCredential networkCredential = new NetworkCredential(SmtpUserName, SmtpPassword);
                    //    client.Credentials = networkCredential;
                    //}
                    MailMessage mailMessage = new MailMessage();
                    bool containsError = false;
                    if (File.ReadAllText(fileName,Encoding.Unicode).Contains("Error:"))
                    {
                        containsError = true;
                    }
                    foreach (var emailto in EmailTo.Split(';'))
                    {
                        if (!emailto.Trim().Equals(string.Empty)) mailMessage.To.Add(new MailAddress(emailto));
                    }
                    mailMessage.From = new MailAddress(EmailFrom, "Service Outcome");
                    var subFileName = logFile.Split('_');
                    mailMessage.Subject = "From GetAvailabilityAndPrice" + subFileName[0] + "_" + subFileName[1];
                    mailMessage.Attachments.Add(new Attachment(fileName));
                    string bodyMessage = "Plese check the attached file";
                    if(containsError)
                    {
                        bodyMessage += Environment.NewLine + "There were errors as shown in attached file please check.";
                        mailMessage.Priority = MailPriority.High;

                    }
                    mailMessage.Body = bodyMessage;
                    client.Send(mailMessage);
                }
                retValue = true; // SendEmailResult.CreateSuccess();
            }
            catch (Exception ex)
            {
                retValue = false;
                LogError(ErrorTypes.ExternalProvider, "SendEmail", ex.Message, null);
            }

            return retValue;
        }
        public enum TransportTypeEnum
        {
            SMTP,
            Web
        }

        //public void Send(string Message)
        //{
        //    // SendGrid credentials
        //    var credentials = new NetworkCredential(EmailFrom, SmtpPassword);

        //    // Create the email object first, then add the properties.
        //    //SendGrid.Mail message = SendGrid.Mail.GetInstance();
        //    SendGridMessage sgMessage = new SendGridMessage();

        //    sgMessage.To = new MailAddress[] {new MailAddress("nimesh.trivedi@thlonline.com", "Nimesh")};

        //// Add the message properties.
        //    //message.To = new MailAddress[] { new System.Net.Mail.MailAddress(ToEmail.Trim(), ToName.Trim()) };
        //    sgMessage.From = new MailAddress("nimesh.trivedi@thlonline.com", "Nims");
        //    sgMessage.Subject = "Error from Availablity and price";
        //    //message.From = new System.Net.Mail.MailAddress(FromEmail.Trim(), FromName.Trim());
        //    //message.Subject = Subject.Trim();

        //    // Add the HTML and Text bodies
        //    //message.Html = HTMLBody;
        //    sgMessage.Html = Message;

           
        //    // Sends the message, using the selected transport
        //    SendGrid.ISendGrid aaGrid;
           
        //    SendGrid.ITransport smtpTransport = SendGrid.Transport.SMTP.GetInstance(credentials);
        //        //SendGrid.Transport.SMTP smtpTransport = SendGrid.Transport.SMTP.GetInstance(credentials);
            
        //        smtpTransport.Deliver(message);

            
            

        //}

        public static bool LogXML(string fileStamp, XmlNode xmlNode)
        {
            if (EventLogOn == false) return false;
            System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
            try
            {
                sw.WriteLine(xmlNode.OuterXml);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                sw.Close();
            }

        }

        public static bool LogXML(string fileStamp, XmlDocument xmldocument)
        {
            if (EventLogOn == false) return false;
            System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + fileStamp + ".xml");
            try
            {
                sw.WriteLine(xmldocument.OuterXml);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                sw.Close();
            }
        }

        public static bool LogEventFile(EventTypes evType, string evParams)
        {
            //if (EventLogOn == false) return false;
            //System.IO.StreamWriter sw = System.IO.File.AppendText(eventPath + "AvailabilityAndPrice.evtx");
            //try
            //{
            //    string logLine = DateTime.Now + ", " + evType + ", " + evParams;
            //    sw.WriteLine(logLine);
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}
            //finally
            //{
            //    sw.Close();
            //}

            if (EventLogOn == false) return false;
            string fileName = eventPath + "AvailabilityAndPriceData.evtx";
            string logLine = DateTime.Now + ", " + evParams;
            while (CheckIfFileIsOpen(fileName))
            {
                // do nothing
            }
            System.IO.StreamWriter sw = null;
            try
            {
                sw = System.IO.File.AppendText(fileName);
                sw.WriteLineAsync(logLine);
                sw.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }

        }

        public static bool LogEventFileNew(EventTypes evType, string evParams) {
            if (EventLogOn == false) return false;
            string fileName = eventPath + "AvailabilityAndPriceData.evtx";
            string logLine = DateTime.Now + ", " + evParams;
            while (CheckIfFileIsOpen(fileName))
            {
                // do nothing
            }
            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Append)))
            {
                writer.Write(logLine);
            }
            return true;
        }
        public static bool LogEvent(EventLogEntryType evType, string evMessage)
        {
            if (EventLogOn == false) return false;
            //string sSource = "GetAvailabilityAndPrice";
            //string strLogName = sSource;
            //bool LogCreated = CreateLog(strLogName);
            
            //WriteToEventLog(strLogName, sSource, evMessage);
            //EventLog eventLog1 = new EventLog();
            //if (!System.Diagnostics.EventLog.SourceExists("AvailabilityAndPriceDataSourse"))
            //{
            //    System.Diagnostics.EventLog.CreateEventSource("AvailabilityAndPriceDataSourse",
            //                                                          "AvailabilityAndPriceData");
            //}

            //eventLog1.Source = "AvailabilityAndPriceDataSourse";
            //// the event log source by which 

            ////the application is registered on the computer

            //eventLog1.Log = "AvailabilityAndPriceData";
            //eventLog1.WriteEntry(evMessage);

            string sSource = "GetAvailabilityAndPrice";
            //string strLogName = sSource;
            //bool LogCreated = CreateLog(strLogName);
            try
            {
                if (!EventLog.SourceExists(sSource))
                    EventLog.CreateEventSource(sSource, sSource);
                EventLog.WriteEntry(sSource, evMessage, evType);
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
            }
            finally { }
            return true;
        }

        private static void WriteToEventLog(string strLogName, string strSource, string strErrDetail)
        {
            System.Diagnostics.EventLog SQLEventLog = new System.Diagnostics.EventLog();

            try
            {
                if (!System.Diagnostics.EventLog.SourceExists(strLogName))
                    CreateLog(strLogName);


                SQLEventLog.Source = strLogName;
                SQLEventLog.WriteEntry(Convert.ToString(strSource)
                                      + Convert.ToString(strErrDetail),
                EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                SQLEventLog.Source = strLogName;
                SQLEventLog.WriteEntry(Convert.ToString("INFORMATION: ")
                                      + Convert.ToString(ex.Message),
                EventLogEntryType.Information);
            }
            finally
            {
                SQLEventLog.Dispose();
                SQLEventLog = null;
            }
        }

        private static bool CreateLog(string strLogName)
        {
            bool Result = false;

            try
            {
                    System.Diagnostics.EventLog.CreateEventSource(strLogName, strLogName);
                    System.Diagnostics.EventLog SQLEventLog = 
				new System.Diagnostics.EventLog();

                    SQLEventLog.Source = strLogName;
                    SQLEventLog.Log = strLogName;

                    SQLEventLog.Source = strLogName;
                    SQLEventLog.WriteEntry("The " + strLogName + " was successfully initialize component.", EventLogEntryType.Information);

                    Result = true;
            }
            catch
            {
                Result = false;
            }

            return Result;

        }

        /// <summary>
        /// Log Quote information 
        /// TODO: define log file path
        /// </summary>
        /// <param name="fileStamp"></param>
        /// <param name="xmldocument"></param>
        /// <returns>write success status</returns>

        public static int GetObjectKBSize(object obj)
        {
            return System.Runtime.InteropServices.Marshal.SizeOf(obj);
        }

        /// <summary>
        /// Quote a Name Value Collection to JSON 
        /// </summary>
        /// <param name="nvc"></param>
        /// <returns></returns>
        public static string DisplayNameValueParams(NameValueCollection nvc)
        {
            StringBuilder sb = new StringBuilder("{");
            foreach (string key in nvc.AllKeys)
            {
                sb.Append(key + ":" + nvc[key] + ",");
            }
            return sb.ToString().TrimEnd(',') + "}";
        }
        // Added
        public static async void ProcessWrite(ErrorTypes errType, string errMethod, string errStr, string errParams, string fileName)
        {
            string filePath = logPath + fileName + ".log";
            string text = DateTime.Now + ", " + errType + ", " + errMethod + ", " + errStr + ", " + errParams;
            await WriteTextAsync(filePath, text);
        }

        private static async Task WriteTextAsync(string filePath, string text)
        {
            if (ErrorLogOn == false) return;

            byte[] encodedText = Encoding.Unicode.GetBytes(text);

            using (FileStream sourceStream = new FileStream(filePath,
                FileMode.Append, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            };
        }
        public static bool ProcessWriteMult(ErrorTypes errType, string errMethod, string errStr, string errParams,string fileNam)
        {
            string fileName = logPath + logFile;
            string logLine = DateTime.Now + ", " + errType + ", " + errMethod + ", " + errStr + ", " + errParams;
            while (CheckIfFileIsOpen(fileName))
            {
                // do nothing
            }
            System.IO.StreamWriter sw = null; 
            try
            {
                sw = System.IO.File.AppendText(fileName);
                
                sw.WriteLineAsync(logLine);
                sw.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            finally
            {
                if(sw != null)
                sw.Close();
            }
        }

        public static bool ProcessWriteMult(string errMsg, string passedFileName="")
        {
            string fileName = logPath +  (passedFileName.Trim().Equals(string.Empty) ? logFile : passedFileName);
            byte[] encodedText = Encoding.Unicode.GetBytes(errMsg);
            FileStream sourceStream = null;
            try
            {
                sourceStream = new FileStream(fileName,
                   FileMode.Append, FileAccess.Write, FileShare.ReadWrite,
                   bufferSize: 4096, useAsync: true);
                sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
            }
                catch(System.IO.IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sourceStream.Flush();
                sourceStream.Close();
                sourceStream.Dispose();
            }
            return true;
        }
        private static bool CheckIfFileIsOpen(string fileName)
        {
            if (!File.Exists(fileName)) return false;

            try
            {
                File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (System.IO.IOException exp)
            {
                return true;
            }
            return false;
        }

        //End Added
    }
}
