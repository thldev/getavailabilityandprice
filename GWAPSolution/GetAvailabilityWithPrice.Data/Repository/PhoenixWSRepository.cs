﻿using GetAvailabilityWithPrice.Data.Factory;
using GetAvailabilityWithPrice.Data.Models;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace GetAvailabilityWithPrice.Data.Repository
{
    public class PhoenixWSRepository : IPhoenixWSRepository
    {
        #region Private Members and Properties
        public IList<XmlNode> ReturnDocs { get; set; }
        public string WSUrl { get; private set; }

        //private StringBuilder emailBody = new StringBuilder();
        #endregion
        public PhoenixWSRepository()
        {
            ReturnDocs = new List<XmlNode>();
            WSUrl = ConfigurationManager.AppSettings["AuroraAvailabilityURL"];
            //emailBody = new StringBuilder();
        }
        public IEnumerable<XmlNode> FetchAllDocs(DateTime startDate, DateTime endDate, IEnumerable<ParametersEntity> parameters)
        {

            PhoenixWebService lPWS = new PhoenixWebService();
            //lPWS.Url = "http://test-app-001/Phoenix%20webservice/service.asmx";
            lPWS.Url = WSUrl; // ConfigurationManager.AppSettings["AuroraServiceBaseURL"];

           
            bool isTestMode = bool.Parse(ConfigurationManager.AppSettings["IsTestMode"]);
            string AgnisInclusiveProduct = "false";
            string vCodeStr = string.Empty;
            short numberOfAdults = short.Parse(ConfigurationManager.AppSettings["NumberOfAdults"]);
            short numberOfChildren = short.Parse(ConfigurationManager.AppSettings["NumberOfChildren"]);
            string packageCode = null;
            bool isVan = bool.Parse(ConfigurationManager.AppSettings["IsVan"]);
            string countryOfResidence = null;
            bool isGross = bool.Parse(ConfigurationManager.AppSettings["IsGross"]);
            bool isBestBuy = bool.Parse(ConfigurationManager.AppSettings["IsBestBuy"]);

            var n2 = from p in parameters
                     from b in p.BrandList
                     from l in b.LocationList
                     select new
                     {
                         AgentCode = p.AgentCode,
                         CountryCode = p.Countrycode,
                         BrandCode = b.BrandCode,
                         LocationFrom = l.LocationFrom,
                         LocationTo = l.LocationTo
                     };
            List<IAsyncResult> ar = new List<IAsyncResult>();// = new AyncResult[arrayLength];
            List<WaitHandle> w = new List<WaitHandle>();

            foreach (var para in n2)
            {
                AsyncCallback cb = new AsyncCallback(GetAvailabilityCompleted);
                IAsyncResult aResult = lPWS.BeginGetAvailability(para.BrandCode, para.CountryCode,
                    vCodeStr, para.LocationFrom, startDate, para.LocationTo, endDate,
                    numberOfAdults, numberOfChildren, para.AgentCode, packageCode,
                    isVan, isBestBuy, countryOfResidence, isTestMode, isGross, cb, new StateClass { BrandCode = para.BrandCode, CountryCode = para.CountryCode, LocationFrom = para.LocationFrom, StartDate = startDate, LocationTo = para.LocationTo, EndDate = endDate });
                ar.Add(aResult);

                var asyncHandle = aResult.AsyncWaitHandle;
                if (w.Count > 63)
                {
                    asyncHandle.WaitOne();
                }
                else
                    w.Add(aResult.AsyncWaitHandle);
            }


            WaitHandle.WaitAll(w.ToArray());

            //if (!emailBody.ToString().Equals(string.Empty))
            //{
            //    //THLDebugRepository.ProcessWriteMult(emailBody.ToString());
            //    THLDebugRepository.SendEmail(emailBody.ToString());
            //}
            return ReturnDocs;

        }

        private void GetAvailabilityCompleted(IAsyncResult ar)
        {
            var a = ((StateClass)(ar.AsyncState));
            PhoenixWebService result = new PhoenixWebService();
            XmlNode xNode = null;
            try
            {
                xNode = result.EndGetAvailability(ar);
            }
            catch (Exception ex)
            {
                string valuesPassed = string.Format("Message : [{6}] : Failed to call webservice with values Brand=[{0}],Country=[{1}],LocFrom=[{2}],LocTo=[{3}],StartDate=[{4}],EndDate=[{5}]", a.BrandCode, a.CountryCode, a.LocationFrom, a.LocationTo, a.StartDate, a.EndDate, ex.Message);
                string fileName = a.BrandCode + "-" + a.CountryCode + "-" + a.LocationFrom + "_" + a.LocationTo + "_" + a.StartDate.ToString("ddMMMyyyy") + "_" + a.EndDate.ToString("ddMMMyyyy") + DateTime.Now.ToString("ddMMMyyyyhhmmss");
                //emailBody.Append(valuesPassed + "\n");
                THLDebugRepository.ProcessWriteMult(DateTime.Now + ", WebService Error: Error Retriving Data From WebService" + "," + valuesPassed + Environment.NewLine);
                return;
            }
            XmlDocument returnedDocument = new XmlDocument();

            XmlElement createdElm = returnedDocument.CreateElement("Datas");
            createdElm.InnerXml = xNode.InnerXml;

            returnedDocument.AppendChild(createdElm);

            XmlNode root = returnedDocument.DocumentElement;

            XmlElement elem = returnedDocument.CreateElement("CountryCode");
            elem.InnerText = a.CountryCode;
            root.AppendChild(elem);

            elem = returnedDocument.CreateElement("LocationFrom");
            elem.InnerText = a.LocationFrom;
            root.AppendChild(elem);

            elem = returnedDocument.CreateElement("StartDate");
            elem.InnerText = a.StartDate.ToString("dd-MM-yyyy");
            root.AppendChild(elem);

            elem = returnedDocument.CreateElement("LocationTo");
            elem.InnerText = a.LocationTo;
            root.AppendChild(elem);

            elem = returnedDocument.CreateElement("EndDate");
            elem.InnerText = a.EndDate.ToString("dd-MM-yyyy");
            root.AppendChild(elem);

            ReturnDocs.Add(returnedDocument);

            //lPWS.EndGetAvailability(ar);
        }

        //private async void PerformLogEntry(ErrorTypes errorTypes, string module, string valuesPassed, string p2, string fileName)
        //{
        //    Task<bool> task1 = new Task<bool>
        //         (
        //             () =>
        //             {
        //                 THLDebugRepository.ProcessWriteMult(errorTypes, module, valuesPassed, "None", fileName);
        //                 return true;
        //             }
        //         );

        //    // start the task
        //    task1.Start();

        //    // get the result from the task - analogous to 
        //    // calling t.Result (without the thread blocked, of course)
        //    await task1;
        //    //Console.WriteLine("After await, result = '" + result + "'");
        //    //Console.WriteLine("End StartDate : {0} and EndDate {1}", StartDate, EndDate);
        //}

        //void lPWS_GetAvailabilityCompleted(object sender, GetAvailabilityCompletedEventArgs e)
        //{
        //    XmlNode xNode = e.Result;
        //    ReturnDocs.Add(xNode);
        //}
        //public IEnumerable<XmlNode> FetchAllDocsNew(DateTime StartDate, DateTime EndDate, IEnumerable<ParametersEntity> parameters)
        //{
        //    PhoenixWebService lPWS = new PhoenixWebService();
        //    lPWS.Url = "http://test-app-001/Phoenix%20webservice/service.asmx";
        //    bool isTestMode = bool.Parse(ConfigurationManager.AppSettings["IsTestMode"]);
        //    string AgnisInclusiveProduct = "false";
        //    string vCodeStr = string.Empty;
        //    short numberOfAdults = short.Parse(ConfigurationManager.AppSettings["NumberOfAdults"]);
        //    short numberOfChildren = short.Parse(ConfigurationManager.AppSettings["NumberOfChildren"]);
        //    string packageCode = null;
        //    bool isVan = bool.Parse(ConfigurationManager.AppSettings["IsVan"]);
        //    string countryOfResidence = null;
        //    bool isGross = bool.Parse(ConfigurationManager.AppSettings["IsGross"]);
        //    bool isBestBuy = bool.Parse(ConfigurationManager.AppSettings["IsBestBuy"]);

        //    var n2 = from p in parameters
        //             from b in p.BrandList
        //             from l in b.LocationList
        //             select new
        //             {
        //                 AgentCode = p.AgentCode,
        //                 CountryCode = p.Countrycode,
        //                 BrandCode = b.BrandCode,
        //                 LocationFrom = l.LocationFrom,
        //                 LocationTo = l.LocationTo
        //             };

        //    int arrayLength = ((NumberOfWeeks + 2) * 2 * n2.Count());
        //    int ctr = 0;
        //    List<IAsyncResult> ar = new List<IAsyncResult>();// = new AyncResult[arrayLength];
        //    //WaitHandle[] w = new WaitHandle[64];
        //    List<WaitHandle> w = new List<WaitHandle>();
        //    int wtCtr = 0;

        //    foreach (var para in n2)
        //    {

        //        IAsyncResult aResult = lPWS.BeginGetAvailability(para.BrandCode, para.CountryCode,
        //            vCodeStr, para.LocationFrom, StartDate, para.LocationTo, EndDate,
        //            numberOfAdults, numberOfChildren, para.AgentCode, packageCode,
        //            isVan, isBestBuy, countryOfResidence, isTestMode, isGross, null, new StateClass { CountryCode = para.CountryCode, LocationFrom = para.LocationFrom, StartDate = StartDate, LocationTo = para.LocationTo, EndDate = EndDate });
        //        ar.Add(aResult);
        //        w.Add(aResult.AsyncWaitHandle);
        //        //w[wtCtr] = ar[ctr].AsyncWaitHandle;

        //        ctr++;
        //        wtCtr++;
        //        if (wtCtr >= 64)
        //        {
        //            WaitHandle.WaitAll(w.ToArray());
        //            for (int newCtr = 0; newCtr < wtCtr; newCtr++)
        //            {
        //                //ReturnDocs.Add(lPWS.EndGetAvailability(ar[newCtr]));
        //                object aS = ar[newCtr].AsyncState;
        //                var a = ((StateClass)(ar[newCtr].AsyncState));

        //                XmlNode xNode = lPWS.EndGetAvailability(ar[newCtr]);

        //                XmlDocument returnedDocument = new XmlDocument();

        //                XmlElement createdElm = returnedDocument.CreateElement("Datas");
        //                createdElm.InnerXml = xNode.InnerXml;

        //                returnedDocument.AppendChild(createdElm);


        //                XmlNode root = returnedDocument.DocumentElement;

        //                XmlElement elem = returnedDocument.CreateElement("CountryCode");
        //                elem.InnerText = a.CountryCode;
        //                root.AppendChild(elem);

        //                elem = returnedDocument.CreateElement("LocationFrom");
        //                elem.InnerText = a.LocationFrom;
        //                root.AppendChild(elem);

        //                elem = returnedDocument.CreateElement("StartDate");
        //                elem.InnerText = a.StartDate.ToString("dd-MM-yyyy");
        //                root.AppendChild(elem);

        //                elem = returnedDocument.CreateElement("LocationTo");
        //                elem.InnerText = a.LocationTo;
        //                root.AppendChild(elem);

        //                elem = returnedDocument.CreateElement("EndDate");
        //                elem.InnerText = a.EndDate.ToString("dd-MM-yyyy");
        //                root.AppendChild(elem);

        //                ReturnDocs.Add(returnedDocument);
        //            }
        //            w = new List<WaitHandle>();
        //            ar = new List<IAsyncResult>();
        //            wtCtr = 0;
        //        }
        //    }



        //    WaitHandle.WaitAll(w.ToArray());

        //    for (int newCtr = 0; newCtr < wtCtr; newCtr++)
        //    {
        //        //string CountryCode = ((System.Web.Services.Protocols.SoapHttpClientProtocol.InvokeAsyncState)(((System.Web.Services.Protocols.WebClientAsyncResult)(ar[newCtr])).InternalAsyncState)).Parameters[1];
        //        object aS = ar[newCtr].AsyncState;
        //        var a = ((StateClass)(ar[newCtr].AsyncState));

        //        XmlNode xNode = lPWS.EndGetAvailability(ar[newCtr]);

        //        XmlDocument returnedDocument = new XmlDocument();

        //        XmlElement createdElm = returnedDocument.CreateElement("Datas");
        //        createdElm.InnerXml = xNode.InnerXml;

        //        returnedDocument.AppendChild(createdElm);


        //        XmlNode root = returnedDocument.DocumentElement;

        //        XmlElement elem = returnedDocument.CreateElement("CountryCode");
        //        elem.InnerText = a.CountryCode;
        //        root.AppendChild(elem);

        //        elem = returnedDocument.CreateElement("LocationFrom");
        //        elem.InnerText = a.LocationFrom;
        //        root.AppendChild(elem);

        //        elem = returnedDocument.CreateElement("StartDate");
        //        elem.InnerText = a.StartDate.ToString("dd-MM-yyyy");
        //        root.AppendChild(elem);

        //        elem = returnedDocument.CreateElement("LocationTo");
        //        elem.InnerText = a.LocationTo;
        //        root.AppendChild(elem);

        //        elem = returnedDocument.CreateElement("EndDate");
        //        elem.InnerText = a.EndDate.ToString("dd-MM-yyyy");
        //        root.AppendChild(elem);

        //        ReturnDocs.Add(returnedDocument);
        //        //ReturnDocs.Add(xNode);
        //        //returnDocuments.add(proxy.EndGetAvailabilityData(ar[ctr]));
        //    }
        //    return ReturnDocs;
        //}

        internal class StateClass
        {
            public string BrandCode { get; set; }
            public string CountryCode { get; set; }
            public string LocationFrom { get; set; }
            public string LocationTo { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }
    }
}

