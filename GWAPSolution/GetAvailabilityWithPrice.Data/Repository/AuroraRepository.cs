﻿using GetAvailabilityWithPrice.Data.DataSource;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace GetAvailabilityWithPrice.Data.Repository
{
    public class AuroraRepository : IAuroraRepository
    {
        private readonly AuroraEntities db;
        
        public AuroraRepository()
        {
            db = new AuroraEntities();
        }
      
        public void AvailabilityAndPriceDataInsert(ICollection<AAPDEntity> ConsolidatedList)
        {
            
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,new System.TimeSpan(0,30,0)))
            {
                AuroraEntities context = null;
                try
                {
                    context = new AuroraEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    context.Configuration.ValidateOnSaveEnabled = false;

                    int count = 0;
                    foreach (var entityToInsert in  ConsolidatedList)
                    {
                        ++count;
                        context = AddToContext(context, entityToInsert, count, 200, true);
                    }

                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    THLDebugRepository.ProcessWriteMult(DateTime.Now + ", Database Error: Error Saving Data to database, " + ex.Message + Environment.NewLine);
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }
                scope.Complete();
            }
        }

        private AuroraEntities AddToContext(AuroraEntities context, AAPDEntity entityToInsert, int count, int commitCount, bool recreateContext)
        {
            AvailabilityAndPriceData aAPD = new AvailabilityAndPriceData();
            string userValue = entityToInsert.Countrycode + "_" + GetBrandName(entityToInsert.BrandCode);
            aAPD.AddDateTime = DateTime.Now;
            
            aAPD.AddPrgmName = userValue + " Service";
            aAPD.AddUsrId = userValue;
            aAPD.AvaPriAvaCtyCode = entityToInsert.Countrycode;
            aAPD.AvaPriBestDailyPrice = entityToInsert.BestDailyPrice;
            aAPD.AvaPriBrandCode = entityToInsert.BrandCode;
            aAPD.AvaPriFromDate = entityToInsert.FromDateTime;
            aAPD.AvaPriFromLocCode = entityToInsert.FromLocCode;
            aAPD.AvaPriMessage = entityToInsert.PriMessage;
            aAPD.AvaPriToDate = entityToInsert.ToDateTime;
            aAPD.AvaPriToLocCode = entityToInsert.ToLocCode;
            aAPD.AvaPriVehCode = entityToInsert.VehicleCode;
            aAPD.IntegrityNo = 1;
            aAPD.AvaPriIsAvailable = entityToInsert.IsAvailable;
            aAPD.AvaPriHirePeriod = entityToInsert.HirePeriod;
            try
            {
                context.Set<AvailabilityAndPriceData>().Add(aAPD);
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                THLDebugRepository.ProcessWriteMult(DateTime.Now + ", Database Error: Error Saving Data to database, " + ex.Message + Environment.NewLine);
            }
            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuroraEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    context.Configuration.ValidateOnSaveEnabled = false;
                }
            }

            return context;
        }

        private string GetBrandName(string passedValue)
        {
            string retValue = string.Empty;
            switch (passedValue.ToUpper())
            {
                case "A":
                    retValue = ConfigurationManager.AppSettings["ABrandName"];
                    break;
                case "B":
                    retValue = ConfigurationManager.AppSettings["BBrandName"];
                    break;
                case "E":
                    retValue = ConfigurationManager.AppSettings["EBrandName"];
                    break;
                case "M":
                    retValue = ConfigurationManager.AppSettings["MBrandName"];
                    break;
                case "Q":
                    retValue = ConfigurationManager.AppSettings["QBrandName"];
                    break;
                case "U":
                    retValue = ConfigurationManager.AppSettings["UBrandName"];
                    break;
                case "Y":
                    retValue = ConfigurationManager.AppSettings["YBrandName"];
                    break;
            }
            return retValue;
        }
    }
}
