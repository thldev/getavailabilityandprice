﻿using GetAvailabilityWithPrice.Domain.Services;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GetAvailabilityWithPrice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Time : " + DateTime.Now.ToString());
            //INotificationService nService;
            //ILoggingService lService = new LoggingService();
            //lService.LogEvent(System.Diagnostics.EventLogEntryType.Information, "Starting Service at " + DateTime.Now.ToString());
            //lService.LogEventFile(Infrastructure.Entities.EventTypes.HTTPRequest, "Starting Service at " + DateTime.Now.ToString());
            CreateMultipleTasks();
            Console.WriteLine("Ending Time : " + DateTime.Now.ToString());
            Console.ReadLine();
        }

        private static void CreateMultipleTasks()
        {
            DateTime secondRunStartDate = DateTime.Now.AddDays(2);
            DateTime secondRunEndDate = secondRunStartDate.AddDays(7);
            AvailabilityAndPriceDataService AAPDService = new AvailabilityAndPriceDataService("Y");
            AAPDService.PerformFetchAndPopulateDBTask();
        }
    }
}
