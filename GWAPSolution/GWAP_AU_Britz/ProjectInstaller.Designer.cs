﻿namespace GWAP_AU_Britz
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AU_BritzProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.AU_Britz_ServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // AU_BritzProcessInstaller
            // 
            this.AU_BritzProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.AU_BritzProcessInstaller.Password = null;
            this.AU_BritzProcessInstaller.Username = null;
            // 
            // AU_Britz_ServiceInstaller
            // 
            this.AU_Britz_ServiceInstaller.Description = "[AU-Britz] Get Availability And Price";
            this.AU_Britz_ServiceInstaller.DisplayName = "AU_Britz";
            this.AU_Britz_ServiceInstaller.ServiceName = "AU_Britz";
            this.AU_Britz_ServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.AU_Britz_ServiceInstaller,
            this.AU_BritzProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller AU_BritzProcessInstaller;
        private System.ServiceProcess.ServiceInstaller AU_Britz_ServiceInstaller;
    }
}