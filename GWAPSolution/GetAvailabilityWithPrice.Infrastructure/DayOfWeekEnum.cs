﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure
{
    static class DayOfWeekEnum
    {
        static DayOfWeek StringToEnum(string Value)
        {
            Type t = typeof (DayOfWeek); 
            foreach (FieldInfo fi in t.GetFields())
                if (fi.Name == Value)
                    return (DayOfWeek)fi.GetValue(null);    // We use null because
            // enumeration values
            // are static

            throw new Exception(string.Format("Can't convert {0} to {1}", Value, t.ToString()));
        }

    }
}
