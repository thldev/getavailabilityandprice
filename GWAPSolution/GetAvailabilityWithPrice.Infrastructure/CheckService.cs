using System;
using System.Configuration;
using System.IO;
using System.Text;
using GetAvailabilityWithPrice.Infrastructure.Services;

namespace GetAvailabilityWithPrice.Infrastructure
{
    public class CheckService
    {
        public string CheckMessage { get; set; }

        public CheckService()
        {
            CheckMessage = string.Empty;
        }
        public bool CheckDayAndTime()
        {
            bool retValue = false;
            //DateTime startTime = DateTime.Now;
            DateTime startTime = ConfigurationManager.AppSettings["CurrentDatetime"] == null ? DateTime.Now : DateTime.Parse(ConfigurationManager.AppSettings["CurrentDatetime"]);
            try
            {
                var runDay = ConfigurationManager.AppSettings["RunDays"];
                var runHour = int.Parse(ConfigurationManager.AppSettings["RunTime"]);

                if (runHour < 0 || runHour > 23)
                {
                    CheckMessage = string.Format("Value set for hour [{0}] is outside allowed range 0 to 23", runHour);
                }
                //WriteToLog("this is from new settings");
                else
                    foreach (var thisDay in runDay.Split(','))
                    {
                        //if (Enum.IsDefined(typeof (DayOfWeek), thisDay.Trim()))
                        //{
                        //    if (startTime.DayOfWeek.Equals((DayOfWeek)Enum.Parse(typeof(DayOfWeek), thisDay.Trim(), true)) && startTime.TimeOfDay.Hours == runHour) 
                        //    {
                        //        retValue = true;
                        //        return retValue;
                        //    }
                        //}
                        if (startTime.DayOfWeek.ToString().ToUpper().Equals(thisDay.Trim().ToUpper()) && startTime.TimeOfDay.Hours == runHour)
                        {
                            retValue = true;
                            break;
                        }
                    }
            }
            catch(Exception ex)
            {
                CheckMessage = ex.Message;
            }
            return retValue;
        }

        public static bool WriteToLog(string errMsg)
        {
            string logPath = ConfigurationManager.AppSettings["logPath"];
            string fileName = logPath + "AvailabilityAndPriceData.log";
            byte[] encodedText = Encoding.Unicode.GetBytes(errMsg);
            FileStream sourceStream = null;
            try
            {
                sourceStream = new FileStream(fileName,
                   FileMode.Append, FileAccess.Write, FileShare.ReadWrite,
                   bufferSize: 4096, useAsync: true);
                //Task theTask = sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
                sourceStream.WriteAsync(encodedText, 0, encodedText.Length);
                
            }
            catch (System.IO.IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                sourceStream.Flush();
                sourceStream.Close();
                sourceStream.Dispose();
            }
            return true;
        }
        public static bool CheckDayAndTimeOld()
        {
            bool retValue = false;
            DateTime startTime = DateTime.Now;

            //if (Enum.IsDefined(typeof(DayOfWeek), strDayofWeekFromXml))
            //{
            //    BuildingOpenDate.DayOfWeek = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), strDayofWeekFromXml, true);
            //}
            //else
            //{
            //    // do some logic here
            //}

            if (startTime.DayOfWeek.Equals(DayOfWeek.Wednesday) || startTime.DayOfWeek.Equals(DayOfWeek.Thursday) ||
                startTime.DayOfWeek.Equals(DayOfWeek.Friday))
            {
                if (startTime.TimeOfDay.Hours == 10)
                    retValue = true;
            }
            return retValue;
        }
    }
}