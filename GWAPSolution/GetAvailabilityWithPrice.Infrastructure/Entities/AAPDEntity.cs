﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Entities
{
    public abstract class AAPDEntity
    {
        public long PriID { get; set; }
        public string Countrycode { get; set; }
        public string BrandCode { get; set; }
        public string VehicleCode { get; set; }
        public string FromLocCode { get; set; }
        public string ToLocCode { get; set; }
        public string PriMessage { get; set; }
        public string AddUserID { get; set; }
        public string ModUserID { get; set; }
        public string AddProgramName { get; set; }
        public DateTime FromDateTime { get; set; }
        public DateTime ToDateTime { get; set; }
        public decimal BestDailyPrice { get; set; }
        public byte IntegrityNo { get; set; }
        public bool IsAvailable { get; set; }
        public int HirePeriod { get; set; }
    }
}
