﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Entities
{
    public abstract class ParametersEntity
    {
        public string Countrycode { get; set; }
        public string AgentCode { get; set; }
        public IEnumerable<BrandEntity> BrandList { get; set; }

        public ParametersEntity()
        {
            // TODO: Complete member initialization
        }

        public ParametersEntity(string countryCode, string agentCode , IList<BrandEntity> brandList)
        {
            Countrycode = countryCode;
            AgentCode = agentCode;
            BrandList = brandList;
        }
    }
}
