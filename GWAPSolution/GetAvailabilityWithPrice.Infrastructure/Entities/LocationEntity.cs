﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetAvailabilityWithPrice.Infrastructure.Entities
{
    public abstract class LocationEntity
    {
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
    }
}
