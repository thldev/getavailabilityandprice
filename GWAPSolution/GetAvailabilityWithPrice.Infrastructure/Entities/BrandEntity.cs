﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Entities
{
    public abstract class BrandEntity
    {
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public IEnumerable<LocationEntity> LocationList { get; set; }
    }
}
