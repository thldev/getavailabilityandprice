﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Entities
{
    public enum ErrorTypes
    {
        Application = 0,
        Database = 1,
        ExternalProvider = 2,
        Presentation = 3
    }

    public enum EventTypes
    {
        DBRequest = 0,
        CacheRequest = 1,
        ClearCache = 2,
        TimeMeasurement = 3,
        MTierStatus = 4,
        HTTPRequest
    }
}
