﻿using GetAvailabilityWithPrice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Services
{
    public interface IAvailabilityAndPriceDataService
    {
        IEnumerable<AAPDEntity> AAPDList { get; set; }
        //bool PopulateAvailabilityAndPriceData();
        //bool PopulateAvailabilityAndPriceDataAsyncNew();
        bool SaveAvailabilityAndPriceData();
        void PerformFetchAndPopulateDBTask();

        //Task<bool> PopulateAvailabilityAndPriceDataAsync();
    }
}
