﻿using GetAvailabilityWithPrice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Services
{
    public interface IPhoenixWSService
    {
        //IList<AAPDEntity> FetchDataFromService();
        //IList<AAPDEntity> FetchDataFromServiceAsync();
        //IEnumerable<AAPDEntity> CreateMultipleTasksAsync();
        IEnumerable<AAPDEntity> FetchDataFromService(DateTime startDate, DateTime endDate, IEnumerable<ParametersEntity> parameters);
    }
}
