﻿using GetAvailabilityWithPrice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GetAvailabilityWithPrice.Infrastructure.Services
{
    public interface ILoggingService
    {
        bool LogError(ErrorTypes errType, string errMethod, string errStr, string errParams);
        bool LogXML(string fileStamp, XmlNode xmlNode);
        bool LogXML(string fileStamp, XmlDocument xmldocument);
        bool LogEventFile(EventTypes evType, string evParams);
        bool LogEvent(EventLogEntryType evType, string evMessage);
        bool ProcessWriteMult(string errMsg, string passedFileName="");
        bool ProcessWriteMult(ErrorTypes errType, string errMethod, string errStr, string errParams, string fileNam);
        bool SendOutcomeEmail();
        bool SendEmail(string MessageBody);


    }
}
