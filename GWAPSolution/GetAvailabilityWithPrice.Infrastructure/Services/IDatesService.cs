﻿using GetAvailabilityWithPrice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Services
{
    public interface IDatesService
    {
        void BuildDatesList();
        IEnumerable<DatesEntity> FetchDatesList();
    }
}
