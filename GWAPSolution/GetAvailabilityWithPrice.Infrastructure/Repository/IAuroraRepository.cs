﻿using GetAvailabilityWithPrice.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Repository
{
    public interface IAuroraRepository
    {
        //long AvailabilityAndPriceDataInsert(string avaPriAvaCtyCode, string aAvaPriBrandCode, string avaPriVehCode, string avaPriFromLocCode, DateTime avaPriFromDate,
        //    string avaPriToLocCode, DateTime avaPriToDate, string avaMessage, bool isAvailable, int hirePeriod, decimal avaPriBestDailyPrice, byte integrityNo, string addUsrId, string addPrgmName);

        void AvailabilityAndPriceDataInsert(ICollection<AAPDEntity> ConsolidatedList);
    }
}
