﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;
using GetAvailabilityWithPrice.Infrastructure.Entities;

namespace GetAvailabilityWithPrice.Infrastructure.Repository
{
    public interface IDebugRepository
    {
        bool LogError(ErrorTypes errType, string errMethod, string errStr, string errParams);
        bool LogXML(string fileStamp, XmlNode xmlNode);
        bool LogXML(string fileStamp, XmlDocument xmldocument);
        bool LogEvent(EventTypes evType, string evParams);
    }
}
