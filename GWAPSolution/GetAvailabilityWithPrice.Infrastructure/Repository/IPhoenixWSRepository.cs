﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace GetAvailabilityWithPrice.Infrastructure.Repository
{
    public interface IPhoenixWSRepository
    {
        //XmlNode FetchDataFromService(string brand, string countryCode, string vehicleCode, string checkOutZoneCode,
        //                               DateTime checkOutDateTime, string checkInZoneCode, DateTime checkInDateTime, short numberOfAdults, short numberOfChildren,
        //                               string agentCode, string packageCode, bool isVan, bool isBestBuy, string countryOfResidence, bool isTestMode, bool isGross, bool agnisInclusiveProduct);

        //IEnumerable<XmlNode> FetchAllDocs(DateTime startDate, DateTime endDate);

        IEnumerable<XmlNode> FetchAllDocs(DateTime StartDate, DateTime EndDate, IEnumerable<Entities.ParametersEntity> parameters);
    }
}
