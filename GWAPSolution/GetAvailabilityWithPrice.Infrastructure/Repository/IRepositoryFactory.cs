﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Infrastructure.Repository
{
    public interface IRepositoryFactory
    {
        //IParametersRepository GetParametersRepository();
        IParametersRepository GetParametersRepository(string paramKey);
        IAuroraRepository GetAuroraRepository();
        IPhoenixWSRepository GetPhoenixWSRepository();
    }
}
