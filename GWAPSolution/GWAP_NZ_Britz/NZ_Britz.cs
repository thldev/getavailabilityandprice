﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using GetAvailabilityWithPrice.Data.Repository;
using GetAvailabilityWithPrice.Domain.Services;
using GetAvailabilityWithPrice.Infrastructure;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Services;

namespace GWAP_NZ_Britz
{
    public partial class NZ_Britz : ServiceBase
    {
        private Timer serviceTimer;
        public NZ_Britz()
        {
            serviceTimer = new System.Timers.Timer();
            //Check Every 60 minutes
            serviceTimer.Interval = 60 * 60 * 1000; //(10000) * 6;
            serviceTimer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsed);
            serviceTimer.Enabled = true;
            serviceTimer.Start();
            InitializeComponent();
        }
        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            CreateMultipleTasks();
        }
        protected override void OnStart(string[] args)
        {
            CreateMultipleTasks();
        }

        static string lastmessage = string.Empty;
        public void CreateMultipleTasks()
        {
            //string brandCode = "B";
            //IAvailabilityAndPriceDataService AAPDService = new AvailabilityAndPriceDataService(brandCode);
            //AAPDService.PerformFetchAndPopulateDBTask();
            ConfigurationManager.RefreshSection("appSettings");
            DateTime startTime = ConfigurationManager.AppSettings["CurrentDatetime"] == null ? DateTime.Now : DateTime.Parse(ConfigurationManager.AppSettings["CurrentDatetime"]);
            
            try
            {
                ILoggingService lService = new LoggingService();
                THLDebugRepository.ProcessWriteMult(startTime + " : Hourly check for Britz[NZ]" + Environment.NewLine, "HourlyCheck.log");

                CheckService cs = new CheckService();
                if (cs.CheckDayAndTime())
                {
                    //THLDebugRepository.ProcessWriteMult(startTime + "," + ErrorTypes.Application + ", Starting Fetch method now" + Environment.NewLine);
                    THLDebugRepository.SetFileName();
                    const string brandCode = "B";
                    IAvailabilityAndPriceDataService aapdService = new AvailabilityAndPriceDataService(brandCode);
                    aapdService.PerformFetchAndPopulateDBTask();
                    THLDebugRepository.SendOutcomeEmail();
                }
                else
                    if ((!cs.CheckMessage.Equals(string.Empty)) && (!lastmessage.Equals(cs.CheckMessage)))
                    {
                        lastmessage = cs.CheckMessage;
                        THLDebugRepository.SendEmail(cs.CheckMessage + " for Britz[NZ]. Was it intentional?");
                    }
            }
            catch (Exception ex)
            {
                THLDebugRepository.SendEmail(startTime + "," + ErrorTypes.Application + ", Error in CreateMultipleTasks [Britz_NZ], Exception Message" + ex.Message + Environment.NewLine);
                //THLDebugRepository.ProcessWriteMult(startTime + "," + ErrorTypes.Application + ", Error in CreateMultipleTasks [Britz_NZ], Exception Message" + ex.Message + Environment.NewLine);
            }
        }
        protected override void OnStop()
        {
            serviceTimer.Enabled = false;
        }
    }
}
