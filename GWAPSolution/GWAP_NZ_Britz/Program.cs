﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GWAP_NZ_Britz
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new NZ_Britz() 
            //};
            //ServiceBase.Run(ServicesToRun);
#if(!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
	   { 
	        new NZ_Britz(),  
	   };
            ServiceBase.Run(ServicesToRun);
#else
            NZ_Britz myServ = new NZ_Britz();
            myServ.CreateMultipleTasks();
            // here Process is my Service function
            // that will run when my service onstart is call
            // you need to call your own method or function name here instead of Process();
#endif
        }
    }
}
