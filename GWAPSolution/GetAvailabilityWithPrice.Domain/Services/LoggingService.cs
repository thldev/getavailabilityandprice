﻿using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GetAvailabilityWithPrice.Data;
using GetAvailabilityWithPrice.Data.Repository;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using System.Xml;
using System.Diagnostics;
namespace GetAvailabilityWithPrice.Domain.Services
{

    
    public class LoggingService : ILoggingService
    {
        public bool LogError(ErrorTypes errType, string errMethod, string errStr, string errParams)
        {
            return THLDebugRepository.LogError(errType, errMethod, errStr, errParams); 
        }

        public bool LogXML(string fileStamp, XmlNode xmlNode)
        {
            return THLDebugRepository.LogXML(fileStamp, xmlNode);
        }

        public bool LogXML(string fileStamp, XmlDocument xmldocument)
        {
            return THLDebugRepository.LogXML(fileStamp, xmldocument);
        }

        public bool LogEventFile(EventTypes evType, string evParams)
        {
            return THLDebugRepository.LogEventFile(evType, evParams);
        }

        public bool LogEvent(EventLogEntryType evType, string evMessage)
        {
            return THLDebugRepository.LogEvent(evType, evMessage);
        }

        public bool SendEmail(string emailBody)
        {
            return THLDebugRepository.SendEmail(emailBody);
        }





        public bool ProcessWriteMult(string errMsg, string passedFileName = "")
        {
            throw new NotImplementedException();
        }

        public bool ProcessWriteMult(ErrorTypes errType, string errMethod, string errStr, string errParams, string fileNam)
        {
            throw new NotImplementedException();
        }

        public bool SendOutcomeEmail()
        {
            throw new NotImplementedException();
        }
    }
}
