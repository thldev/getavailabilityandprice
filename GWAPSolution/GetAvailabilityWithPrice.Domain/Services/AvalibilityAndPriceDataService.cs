﻿using GetAvailabilityWithPrice.Data.Repository;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GetAvailabilityWithPrice.Data.Factory;
using GetAvailabilityWithPrice.Data.Models;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using System.Configuration;

namespace GetAvailabilityWithPrice.Domain.Services
{
    public class AvailabilityAndPriceDataService : IAvailabilityAndPriceDataService
    {
        

        #region PrivateMembers and Properties
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<AAPDEntity> AAPDList { get; set; }
        public ICollection<AAPDEntity> ConsolidatedList { get; set; }
        private IPhoenixWSService PWSService { get; set; }
        public IAuroraRepository AuroraRepo { get; set; }
        public IPhoenixWSRepository PhoenixRepo { get; set; }
        public IParametersRepository ParamsRepo { get; set; }

        private string brandCode;

        public string BrandCode
        {
            set { 
                brandCode = value;
                ParamKey = "XMLPath";
                switch (brandCode.ToUpper())
                {
                    case "A":
                        //ParamKey = "AXMLPath";
                        BrandName =  ConfigurationManager.AppSettings["ABrandName"];
                        break;
                    case "B":
                        //ParamKey = "BXMLPath";
                        BrandName = ConfigurationManager.AppSettings["BBrandName"];
                        break;
                    case "E":
                        //ParamKey = "BXMLPath";
                        BrandName = ConfigurationManager.AppSettings["EBrandName"];
                        break;
                    case "M":
                        //ParamKey = "MXMLPath";
                        BrandName = ConfigurationManager.AppSettings["MBrandName"];
                        break;
                    case "Q":
                        //ParamKey = "QXMLPath";
                        BrandName = ConfigurationManager.AppSettings["QBrandName"];
                        break;
                    case "U":
                        //ParamKey = "UXMLPath";
                        BrandName = ConfigurationManager.AppSettings["UBrandName"];
                        break;
                    case "Y":
                        //ParamKey = "YXMLPath";
                        BrandName = ConfigurationManager.AppSettings["YBrandName"];
                        break;
                }

            
            }
        }
        

        public string ParamKey { get; set; }
        #endregion
        public AvailabilityAndPriceDataService(string brandCode)
        {
            // TODO: Complete member initialization
            BrandCode = brandCode;
            //ParamKey = paramKey;
            AuroraRepo = RepositoryFactory.Instance.GetAuroraRepository();
            PhoenixRepo = RepositoryFactory.Instance.GetPhoenixWSRepository();
            ParamsRepo = RepositoryFactory.Instance.GetParametersRepository(ParamKey);
            PWSService = new PhoenixWSService();
            ConsolidatedList = new List<AAPDEntity>();
        }

        delegate bool MethodDelegate();
        
        public bool SaveAvailabilityAndPriceData()
        {

            //Console.WriteLine("BulkInsert Starting At {0}", DateTime.Now.ToString());
            AuroraRepo.AvailabilityAndPriceDataInsert(ConsolidatedList);
            //Console.WriteLine("BulkInsert Ending At {0}", DateTime.Now.ToString());
            return true;
        }

        private bool SaveAvailabilityAndPriceData(ICollection<AAPDEntity> listValues)
        {
            AuroraRepo.AvailabilityAndPriceDataInsert(listValues);
            return true;
        }
        
        public void PerformFetchAndPopulateDBTask()
        {
            // Build Dates List
            //string countryCode = string.Empty;
            ILoggingService lService = new LoggingService();
            
            IDatesService Ds = new DatesService();
            Ds.BuildDatesList();
            IEnumerable<DatesEntity> datesList = Ds.FetchDatesList();
            IEnumerable<ParametersEntity> parameters = ParamsRepo.GetParamsList();
            string countryCode = parameters.ElementAt(0).Countrycode;

            lService.LogEvent(System.Diagnostics.EventLogEntryType.Information, "Starting Service for " + BrandName + "[" + countryCode + "]" + " at " + DateTime.Now.ToString());
            THLDebugRepository.ProcessWriteMult(DateTime.Now + "," + ErrorTypes.Application + "," + "StartModule" + ", Starting Service For " + BrandName + "[" + countryCode + "]" +  Environment.NewLine);

            int brkCtr = 0;
            foreach (DatesEntity dE in datesList)
            {
                brkCtr++;
                PWSService = new PhoenixWSService();
                AAPDList = new List<AAPDEntity>();
                AAPDList = PWSService.FetchDataFromService(dE.StartDate, dE.EndDate, parameters);
                //SaveAvailabilityAndPriceData(AAPDList.ToList());
                foreach (AAPDEntity aE in AAPDList)
                    ConsolidatedList.Add(aE);
                THLDebugRepository.ProcessWriteMult(DateTime.Now + "," + ErrorTypes.Application + "," + "StartModule" + ", Fetched " + AAPDList.Count() + " rows of data for " + BrandName + "[" + countryCode + "]" + " startdate: " + dE.StartDate.ToString("dd/MM/yyyy") + " enddate : " + dE.EndDate.ToString("dd/MM/yyyy") + Environment.NewLine);
            }
            DateTime endDateTime = DateTime.Now;
            lService.LogEvent(System.Diagnostics.EventLogEntryType.Information, "Ending Service for " + BrandName + "[" + countryCode + "]" + " at " + DateTime.Now.ToString());
            THLDebugRepository.ProcessWriteMult(DateTime.Now + "," + ErrorTypes.Application + "," + "StartModule" + ", saving to database " + ConsolidatedList.Count() + " rows of data for " + BrandName + "[" + countryCode + "]" + Environment.NewLine);
            SaveAvailabilityAndPriceData();
            THLDebugRepository.ProcessWriteMult(DateTime.Now + "," + ErrorTypes.Application + "," + "StartModule" + ", Ending Service For " + BrandName + "[" + countryCode + "]" + Environment.NewLine);
        }

        public string BrandName { get; set; }
    }
}
