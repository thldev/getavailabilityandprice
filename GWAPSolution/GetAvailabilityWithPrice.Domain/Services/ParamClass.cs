﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetAvailabilityWithPrice.Domain.Services
{
    class ParamClass
    {
        public string Vehiclecode { get; set; }

        public string CountryCode { get; set; }

        public string Brand { get; set; }

        public string CheckOutZoneCode { get; set; }

        public DateTime CheckOutDateTime { get; set; }

        public string CheckInZoneCode { get; set; }

        public DateTime CheckInDateTime { get; set; }

        public string AgentCode { get; set; }

        public string PackageCode { get; set; }

        public bool IsVan { get; set; }

        public short NumberOfAdults { get; set; }

        public short NumberOfChildren { get; set; }

        public bool IsBestBuy { get; set; }

        public string CountryOfResidence { get; set; }

        public string DisplayMode { get; set; }

        public bool IsTestMode { get; set; }

        public bool IsGross { get; set; }

        public bool AgnisInclusiveProduct { get; set; }
    }
}
