﻿using GetAvailabilityWithPrice.Data.Factory;
using GetAvailabilityWithPrice.Data.Models;
using GetAvailabilityWithPrice.Infrastructure;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace GetAvailabilityWithPrice.Domain.Services
{
    public class PhoenixWSService : IPhoenixWSService
    {
        #region PrivateMembers and Properties
        public string BrandStr { get; set; }
        public string CountryCode { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public int HirePeriod { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IPhoenixWSRepository PhoenixRepo { get; set; }
        public IParametersRepository ParamsRepo { get; set; }
        public IParametersService paramsService { get; set; }
        public IList<AAPDEntity> AAPDList { get; set; }
        #endregion
        
        public PhoenixWSService()
        {
            PhoenixRepo = RepositoryFactory.Instance.GetPhoenixWSRepository();
            AAPDList = new List<AAPDEntity>();
        }
        
        private void PopulateAAPDList(XElement retDocument)
        {
            if (retDocument == null) return;



            var result = (from c in retDocument.Descendants("AvailableRate")
                          select c
                          ).ToList();

            foreach (XElement x in result)
            {
                string PriId = string.Empty, PriName, Price = "0", DiscountedRate = "0";/* , Type, Class, IsVeh, ProductInfo, TaxInclude, CurrCode, IsInclusive, AvgRate, UOM, HirePeriod, FlxNum, GrossCalcPercentage, IsCustomerCharge;
                    string TotalSavings, From, To, HirePeriod2, OriginalRate,  GrossAmt, IncludesFreeDay, CodCode, CodDesc, Discount, IsPercentage, DiscountDescription, BlockingRuleAllow, BlockingRuleMessage;
                    string ErrorMessageAllow, ErrorMessageMessage;*/
                string BrandCode = string.Empty;
                string CityCode = string.Empty;
                string FromDateTime = string.Empty;
                string FromLocCode = string.Empty;
                string PriceStatus = string.Empty;
                string ToDateTime = string.Empty;
                string ToLocCode = string.Empty;
                string BlockingRuleAllow = string.Empty;
                string BlockingRuleMessage = string.Empty;
                string ErrorMessageAllow = string.Empty;
                string ErrorMessageMessage = string.Empty;
                bool isAvailable = false;
                //PriceStatus Needs to Be Populated
                //CityCode doubt

                #region PackageElements
                if (x.Element("Package").Attribute("DropOffLocCode") != null)
                    ToLocCode = x.Element("Package").Attribute("DropOffLocCode").Value;

                if (x.Element("Package").Attribute("BrandCode") != null)
                    BrandCode = x.Element("Package").Attribute("BrandCode").Value;

                if (x.Element("Package").Attribute("PickupLocCode") != null)
                {
                    CityCode = x.Element("Package").Attribute("PickupLocCode").Value;
                    FromLocCode = x.Element("Package").Attribute("PickupLocCode").Value;
                }

                //if (x.Element("Package").Attribute("PickupDate") != null)
                //    FromDateTime = x.Element("Package").Attribute("PickupDate").Value;

                //if (x.Element("Package").Attribute("DropOffDate") != null)
                //    ToDateTime = x.Element("Package").Attribute("DropOffDate").Value;
                #endregion
                #region Det Elements

                /// Nimesh Check for IsVeh="1"
                foreach (XElement det in x.Element("Charge").Descendants("Det"))
                {
                    //if (det.Attribute("PrdCode").Value.Equals("WGLOFREE") 
                    //    || det.Attribute("PrdCode").Value.Equals("TOURRAD") 
                    //    || det.Attribute("PrdCode").Value.Equals("CKOSUR")
                    //    || det.Attribute("PrdCode").Value.Equals("ZQNLOC")
                    //    )
                    if (!det.Attribute("IsVeh").Value.Equals("1"))
                        continue;
                    if (det.Attribute("PrdCode") != null)
                        PriId = det.Attribute("PrdCode").Value;
                    if (det.Attribute("PrdName") != null)
                        PriName = det.Attribute("PrdName").Value;

                    //if (det.Attribute("Price") != null)
                    //    Price = det.Attribute("Price").Value;
                    #region Commented Section
                    //if (det.Attribute("Type") != null)
                    //    Type = det.Attribute("Type").Value;

                    //if (det.Attribute("Class") != null)
                    //    Class = det.Attribute("Class").Value;

                    //if (det.Attribute("IsVeh") != null)
                    //    IsVeh = det.Attribute("IsVeh").Value;

                    //if (det.Attribute("ProductInfo") != null)
                    //    ProductInfo = det.Attribute("ProductInfo").Value;

                    //if (det.Attribute("TaxInclude") != null)
                    //    TaxInclude = det.Attribute("TaxInclude").Value;

                    //if (det.Attribute("CurrCode") != null)
                    //    CurrCode = det.Attribute("CurrCode").Value;
                    //if (det.Attribute("IsInclusive") != null)
                    //    IsInclusive = det.Attribute("IsInclusive").Value;

                    //if (det.Attribute("AvgRate") != null)
                    //    AvgRate = det.Attribute("AvgRate").Value;

                    //if (det.Attribute("UOM") != null)
                    //    UOM = det.Attribute("UOM").Value;

                    //if (det.Attribute("HirePeriod") != null)
                    //    HirePeriod = det.Attribute("HirePeriod").Value;

                    //if (det.Attribute("FlxNum") != null)
                    //    FlxNum = det.Attribute("FlxNum").Value;

                    //if (det.Attribute("GrossCalcPercentage") != null)
                    //    GrossCalcPercentage = det.Attribute("GrossCalcPercentage").Value;

                    //if (det.Attribute("IsCustomerCharge") != null)
                    //    IsCustomerCharge = det.Attribute("IsCustomerCharge").Value;

                    if (det.Element("RateBands").HasElements)

                        //if (det.Element("RateBands").Element("Bands").Element("Band").Attribute("From") != null)
                        //    FromLocCode = det.Element("RateBands").Element("Bands").Element("Band").Attribute("From").Value;

                        //if (det.Element("RateBands").Element("Bands").Element("Band").Attribute("To") != null)
                        //    ToLocCode = det.Element("RateBands").Element("Bands").Element("Band").Attribute("To").Value;


                        if (det.Element("RateBands").Element("Bands").Element("Band").Attribute("DiscountedRate") != null)
                            DiscountedRate = det.Element("RateBands").Element("Bands").Element("Band").Attribute("DiscountedRate").Value;

                    #endregion
                }
                #endregion
                #region Error Elements
                BlockingRuleAllow = x.Element("Errors").Element("BlockingRuleMessage").Attribute("Allow").Value;
                BlockingRuleMessage = x.Element("Errors").Element("BlockingRuleMessage").Attribute("Message").Value;
                ErrorMessageAllow = x.Element("Errors").Element("ErrorMessage").Attribute("Allow").Value;
                ErrorMessageMessage = x.Element("Errors").Element("ErrorMessage").Attribute("Message").Value;
                #endregion
                if (!DiscountedRate.Equals("0"))
                {
                    PriceStatus = "Available";
                    isAvailable = true;
                }
                else
                {
                    //string msg = (BlockingRuleMessage.Equals(string.Empty) ? (ErrorMessageMessage.Equals(string.Empty) ? "UnAvailable" : ErrorMessageMessage ): BlockingRuleMessage).Substring(0,64);

                    PriceStatus = (BlockingRuleMessage.Equals(string.Empty) ? (ErrorMessageMessage.Equals(string.Empty) ? "UnAvailable" : ErrorMessageMessage) : BlockingRuleMessage); // "UnAvailable";
                    if (PriceStatus.Length > 64) PriceStatus = PriceStatus.Substring(0, 64);
                }
                //{
                AAPDModel aAPData = new AAPDModel();
                aAPData.AddProgramName = "Client Program";
                aAPData.AddUserID = "Nimesh";
                // aAPData.BestDailyPrice = decimal.Parse(Price);
                aAPData.IsAvailable = isAvailable;
                aAPData.BestDailyPrice = decimal.Parse(DiscountedRate);
                aAPData.BrandCode = BrandCode;
                aAPData.Countrycode = retDocument.Element("CountryCode").Value; //this.CountryCode; // this.CountryCode;// CityCode;
                aAPData.FromDateTime = DateTime.Parse(retDocument.Element("StartDate").Value);// FromDateTime.Equals(string.Empty) ? new DateTime() : DateTime.Parse(FromDateTime);
                aAPData.FromLocCode = retDocument.Element("LocationFrom").Value; //this.LocFrom; // FromLocCode;
                aAPData.IntegrityNo = 1;

                aAPData.PriMessage = PriceStatus;
                aAPData.ToDateTime = DateTime.Parse(retDocument.Element("EndDate").Value); //this.EndDate; // ToDateTime.Equals(string.Empty) ? new DateTime() : DateTime.Parse(ToDateTime);
                aAPData.ToLocCode = retDocument.Element("LocationTo").Value; // this.LocTo; // ToLocCode;
                aAPData.VehicleCode = PriId;
                aAPData.HirePeriod = (aAPData.ToDateTime - aAPData.FromDateTime).Days;
                AAPDList.Add(aAPData);
                //}
            }
        }

        public IEnumerable<AAPDEntity> FetchDataFromService(DateTime startDate, DateTime endDate, IEnumerable<ParametersEntity> parameters)
        {
            IList<XElement> resultDocs = new List<XElement>(); ;
            this.StartDate = startDate;
            this.EndDate = endDate;
            PhoenixRepo = RepositoryFactory.Instance.GetPhoenixWSRepository();
            var test = PhoenixRepo.FetchAllDocs(startDate, endDate, parameters);
            System.Threading.Thread.Sleep(1000);
            foreach (XmlNode currentNode in test.ToList())
            {
                PopulateAAPDList(currentNode.GetXElement());
            }
            return AAPDList;
        }
    }
}
