﻿using GetAvailabilityWithPrice.Domain.Entities;
using GetAvailabilityWithPrice.Infrastructure.Entities;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Domain.Services
{
    public class DatesService :IDatesService
    {
        
        public IList<DatesModel> DatesList { get; set; }
        public int NumberOfWeeks { get; set; }
        public DatesService()
        {
            DatesList = new List<DatesModel>();
        }
        public void BuildDatesList()
        {
            DatesModel dM;
            int firstRunStartHours = int.Parse(ConfigurationManager.AppSettings["FirstRunStartHours"]);
            int secondRunStartHours = int.Parse(ConfigurationManager.AppSettings["SecondRunStartHours"]);

            int firstRunNumberOfDays = int.Parse(ConfigurationManager.AppSettings["FirstRunNumberOfDays"]);
            int secondRunNumberOfDays = int.Parse(ConfigurationManager.AppSettings["SecondRunNumberOfDays"]);

            NumberOfWeeks = int.Parse(ConfigurationManager.AppSettings["NumberOfWeeks"]);

            string pickUpTime = ConfigurationManager.AppSettings["PickUpTime"];
            string dropOffTime = ConfigurationManager.AppSettings["DropOffTime"];


            DateTime firstRunPickUpDate = DateTime.Now.AddHours(firstRunStartHours);
            DateTime firstRunDropOffDate = firstRunPickUpDate.AddDays(firstRunNumberOfDays);

            DateTime firstRunStartDate = DateTime.Parse(firstRunPickUpDate.ToString("dd-MMM-yyyy") + " " + pickUpTime);
            DateTime firstRunEndDate = DateTime.Parse(firstRunDropOffDate.ToString("dd-MMM-yyyy") + " " + dropOffTime);

            dM = new DatesModel();
            dM.StartDate = firstRunStartDate;
            dM.EndDate = firstRunEndDate;
            DatesList.Add(dM);

            DateTime secondRunPickUpDate = DateTime.Now.AddHours(secondRunStartHours);
            DateTime secondRunDropOffDate = firstRunPickUpDate.AddDays(secondRunNumberOfDays);

            DateTime secondRunStartDate = DateTime.Parse(secondRunPickUpDate.ToString("dd-MMM-yyyy") + " " + pickUpTime);
            DateTime secondRunEndDate = DateTime.Parse(secondRunDropOffDate.ToString("dd-MMM-yyyy") + " " + dropOffTime);

            dM = new DatesModel();
            dM.StartDate = secondRunStartDate;
            dM.EndDate = secondRunEndDate;
            DatesList.Add(dM);
            
            for (int loopCtr = 0; loopCtr < NumberOfWeeks; loopCtr++)
            {

                firstRunStartDate = firstRunStartDate.AddDays(7);
                if (firstRunStartDate.DayOfWeek != DayOfWeek.Monday)
                {
                    int rollBackDay = DayOfWeek.Monday - firstRunStartDate.DayOfWeek;
                    firstRunStartDate = firstRunStartDate.AddDays(rollBackDay);
                }
                firstRunEndDate = DateTime.Parse((firstRunStartDate.AddDays(firstRunNumberOfDays)).ToString("dd-MMM-yyyy") + " " + dropOffTime);
                secondRunEndDate = firstRunStartDate.AddDays(secondRunNumberOfDays);

                dM = new DatesModel();
                dM.StartDate = firstRunStartDate;
                dM.EndDate = firstRunEndDate;
                DatesList.Add(dM);

                
                dM = new DatesModel();
                dM.StartDate = firstRunStartDate;
                dM.EndDate = secondRunEndDate;
                DatesList.Add(dM);
            }
        }
        public IEnumerable<DatesEntity> FetchDatesList()
        {
            return DatesList;
        }
    }
}
