﻿using GetAvailabilityWithPrice.Data.Factory;
using GetAvailabilityWithPrice.Infrastructure.Repository;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetAvailabilityWithPrice.Domain.Services
{
    public class ParametersService : IParametersService
    {
        public IParametersRepository ParamsRepo { get; set; }

        //public ParametersService()
        //{
        //    ParamsRepo = RepositoryFactory.Instance.GetParametersRepository();
        //}

        public IEnumerable<Infrastructure.Entities.ParametersEntity> GetParametersList()
        {
            IEnumerable<Infrastructure.Entities.ParametersEntity> retValue = ParamsRepo.GetParamsList();

            return retValue;
        }

       
    }
}
