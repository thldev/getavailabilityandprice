﻿namespace GWAPService
{
    partial class ServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gwapaService1 = new GWAPService.GWAPAService();
            this.gwapbService1 = new GWAPService.GWAPBService();
            this.gwapmService1 = new GWAPService.GWAPMService();
            this.gwapqService1 = new GWAPService.GWAPQService();
            this.gwapuService1 = new GWAPService.GWAPUService();
            this.gwapyService1 = new GWAPService.GWAPYService();
            // 
            // gwapaService1
            // 
            this.gwapaService1.ExitCode = 0;
            this.gwapaService1.ServiceName = "GWAPAService";
            // 
            // gwapbService1
            // 
            this.gwapbService1.ExitCode = 0;
            this.gwapbService1.ServiceName = "GWAPBService";
            // 
            // gwapmService1
            // 
            this.gwapmService1.ExitCode = 0;
            this.gwapmService1.ServiceName = "GWAPMService";
            // 
            // gwapqService1
            // 
            this.gwapqService1.ExitCode = 0;
            this.gwapqService1.ServiceName = "GWAPQService";
            // 
            // gwapuService1
            // 
            this.gwapuService1.ExitCode = 0;
            this.gwapuService1.ServiceName = "GWAPUService";
            // 
            // gwapyService1
            // 
            this.gwapyService1.ExitCode = 0;
            this.gwapyService1.ServiceName = "GWAPYService";

        }

        #endregion

        private GWAPAService gwapaService1;
        private GWAPBService gwapbService1;
        private GWAPMService gwapmService1;
        private GWAPQService gwapqService1;
        private GWAPUService gwapuService1;
        private GWAPYService gwapyService1;
    }
}