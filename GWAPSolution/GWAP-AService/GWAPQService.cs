﻿using GetAvailabilityWithPrice.Domain.Services;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GWAPService
{
    partial class GWAPQService : ServiceBase
    {
        public GWAPQService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CreateMultipleTasks();
        }
        private static void CreateMultipleTasks()
        {
            string brandCode = "Q";
            IAvailabilityAndPriceDataService AAPDService = new AvailabilityAndPriceDataService(brandCode);
            AAPDService.PerformFetchAndPopulateDBTask();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
    }
}
