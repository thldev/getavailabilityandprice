﻿using GetAvailabilityWithPrice.Domain.Services;
using GetAvailabilityWithPrice.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GWAPService
{
    public partial class GWAPAService : ServiceBase
    {
        public GWAPAService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Debugger.Launch();
            CreateMultipleTasks();
        }

        private static void CreateMultipleTasks()
        {
            
            //string paramFileLocation = ConfigurationManager.AppSettings["AXMLPath"];
            
            string brandCode = "A";
            IAvailabilityAndPriceDataService AAPDService = new AvailabilityAndPriceDataService(brandCode);
            AAPDService.PerformFetchAndPopulateDBTask();
            
        }
        protected override void OnStop()
        {
        }
    }
}
